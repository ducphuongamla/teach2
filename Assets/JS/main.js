const btnShowFormLogincheckout = document.querySelector(
  ".accordion__showlogin"
);

if (btnShowFormLogincheckout) {
  const formLogincheckout = document.getElementById("checkout__login-form");
  const divContainerFormLogincheckout = document.querySelector(
    ".accordion__login-form"
  );

  btnShowFormLogincheckout.onclick = () => {
    if (formLogincheckout.style.maxHeight) {
      formLogincheckout.style.maxHeight = null;
      divContainerFormLogincheckout.style.maxHeight = null;
      divContainerFormLogincheckout.style.marginBottom = 0;
    } else {
      formLogincheckout.style.maxHeight = formLogincheckout.scrollHeight + "px";
      divContainerFormLogincheckout.style.maxHeight =
        formLogincheckout.scrollHeight + "px";
      divContainerFormLogincheckout.style.marginBottom = "20px";
    }
  };
}

const btnshowCoupon = document.querySelector(".accordion__showcoupon");

if (btnshowCoupon) {
  const formCoupon = document.querySelector(".coupon__form");
  const divFormCoupon = document.querySelector(".accordion__formcoupon");

  btnshowCoupon.onclick = () => {
    if (formCoupon.style.maxHeight) {
      formCoupon.style.maxHeight = null;
      divFormCoupon.style.maxHeight = null;
    } else {
      formCoupon.style.maxHeight = formCoupon.scrollHeight + "px";
      divFormCoupon.style.maxHeight = formCoupon.scrollHeight + "px";
    }
  };
}

const checkboxCreateAccount = document.querySelector(
  ".account__checkbox--input"
);

if (checkboxCreateAccount) {
  const formCreateAccount = document.querySelector(".create__account--form");

  checkboxCreateAccount.onclick = (e) => {
    if (e.target.checked) {
      formCreateAccount.style.maxHeight = formCreateAccount.scrollHeight + "px";
    } else {
      formCreateAccount.style.maxHeight = null;
    }
  };
}

const checkboxShipToDiffAddress = document.querySelector(
  ".shiptoaddress__checkbox--input"
);

if (checkboxShipToDiffAddress) {
  const formShipToDiffAddress = document.querySelector(".shiptoaddress__form");

  checkboxShipToDiffAddress.onclick = (e) => {
    if (e.target.checked) {
      formShipToDiffAddress.style.maxHeight =
        formShipToDiffAddress.scrollHeight + "px";
    } else {
      formShipToDiffAddress.style.maxHeight = null;
    }
  };
}

const btnsubmit2form = document.querySelector(".method__payment--btn");

if (btnsubmit2form) {
  const formbilling = document.querySelector(".form__billing");
  const formdiffaddress = document.querySelector(".shiptoaddress__form");

  btnsubmit2form.onclick = () => {
    if (checkboxShipToDiffAddress.checked) {
      formbilling.submit();
      formdiffaddress.submit();
    } else {
      formbilling.submit();
    }
  };
}

var coll = document.getElementsByClassName("collapsible");
var i;

if (coll) {
  for (i = 0; i < coll.length; i++) {
    coll[i].addEventListener("click", function () {
      this.classList.toggle("active");
      var content = this.nextElementSibling;
      if (content.style.maxHeight) {
        content.style.maxHeight = null;
      } else {
        content.style.maxHeight = content.scrollHeight + "px";
      }
    });
  }
}

// Get the button
let btnScrollToTop = document.getElementById("btn-go-to-top");

// When the user scrolls down 20px from the top of the document, show the button

if (mybutton) {
  window.onscroll = function () {
    scrollFunction();
  };

  function scrollFunction() {
    if (
      document.body.scrollTop > 20 ||
      document.documentElement.scrollTop > 20
    ) {
      mybutton.style.display = "block";
    } else {
      btnScrollToTop.style.display = "none";
    }
  }

  // When the user clicks on the button, scroll to the top of the document
  function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }
}

// login form

$(document).ready(function () {
  if ($("#loginForm").length) {
    $("#loginForm").validate({
      onfocusout: false,
      onkeyup: false,
      onclick: false,
      rules: {
        user: {
          required: true,
          maxlength: 15,
        },
        password: {
          required: true,
          minlength: 8,
        },
      },
    });
  }
});

$(document).ready(function () {
  if ($(".banner").length) {
    $(".banner").owlCarousel({
      items: 1,
      loop: true,
      autoplay: true,
      dots: true,
      nav: false,
    });
  }
  if ($(".branch-slider").length) {
    $(".branch-slider").owlCarousel({
      loop: true,
      margin: 10,
      nav: false,
      dots: false,
      autoplay: true,
      autoplayTimeout: 3000,
      responsive: {
        0: {
          items: 2,
        },
        600: {
          items: 3,
        },
        1000: {
          items: 5,
        },
      },
    });
  }
  if ($(".new-products-slider").length) {
    $(".new-products-slider").owlCarousel({
      items: 5,
      loop: true,
      autoplay: true,
      dots: true,
      nav: false,
      responsive: {
        0: {
          items: 1,
        },
        600: {
          items: 2,
        },
        1000: {
          items: 4,
        },
        1200: {
          items: 5,
        },
      },
    });
  }
  if ($(".category-product-slider").length) {
    $(".category-product-slider").owlCarousel({
      items: 1,
      nav: false,
      autoplay: true,
      autoplayHoverPause: true,
      autoplayTimeout: 4000,
    });
  }
  if ($(".product-slider").length) {
    $(".product-slider").owlCarousel({
      items: 3,
      loop: true,
      autoplay: true,
      dots: true,
      nav: false,
      responsive: {
        0: {
          items: 2,
        },
        300: {
          items: 1,
        },
        500: {
          items: 2,
        },
        768: {
          items: 3,
        },
        1000: {
          items: 2,
        },
        1200: {
          items: 3,
        },
      },
    });
  }
  if ($(".mostviewed-slider").length) {
    $(".mostviewed-slider").owlCarousel({
      items: 1,
      loop: true,
    });
  }
});
